<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference', 10);
            $table->unsignedInteger('id_client');
            $table->unsignedInteger('id_delivery');
            $table->unsignedInteger('id_state');
            $table->unsignedInteger('id_payment');
            $table->unsignedInteger('id_driver');
            $table->decimal('total_paid', 10, 4);
            $table->date('delivery_date');
            $table->string('delivery_hour',11)->nullable();
            $table->timestamps();
            $table->foreign('id_client')->references('id')->on('clients');
            $table->foreign('id_delivery')->references('id')->on('address');
            $table->foreign('id_state')->references('id')->on('order_state');
            $table->foreign('id_payment')->references('id')->on('payment_type');
            $table->foreign('id_driver')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
