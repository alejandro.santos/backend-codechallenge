<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Address::class, function (Faker $faker) {
    return [
        'client_id' => rand(1,5),
        'firstname' => $faker->name,
        'lastname' 	=> $faker->lastName,
        'phone' 	=> $faker->phoneNumber,
        'country'	=> $faker->stateAbbr,
        'city' 		=>$faker->city,
        'zip' 		=>$faker->postcode,
        'address' 	=>$faker->streetAddress,
    ];
});
