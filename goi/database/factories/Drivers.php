<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Drivers::class, function (Faker $faker) {
    return [
        'firstname' 			=> $faker->name,
        'lastname' 				=> $faker->lastName,
        'vehicle_registration' 	=> rand(1001,9999).' '.Str::random(3),
    ];
});
