<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClientsSeeder::class);

        $this->call(AddressSeeder::class);

        $this->call(DriversSeeder::class);

    	\DB::table('providers')->insert([
            'name' => 'IKEA',
        ]);

        \DB::table('payment_type')->insert([
		    ['name'=>'PayPal'],
		    ['name'=>'TPV'],
		    ['name'=>'Transferencia bancaria'],
		    ['name'=>'Contrareembolso']
		]);

		\DB::table('order_state')->insert([
		    ['name'=>'En espera de pago'],
		    ['name'=>'Pagado'],
		    ['name'=>'En curso'],
		    ['name'=>'Entregado'],
		    ['name'=>'Error en el pago'],
		    ['name'=>'Falta de stock'],
		    ['name'=>'Cancelado']
		]);

		\DB::table('Products')->insert([
		    'name'=>'HYLLESTAD',
		    'description'=>'Colchón muelles ensacados, firme, blanco, 160x200 cm',
		    'isbn'=>'9790404436093',
		    'ean13'=>'4006381333931',
		    'id_provider'=>1,
		    'price'=>399,
		    'stock'=>223
		]);

		\DB::table('orders')->insert([
		    'reference'=> strtoupper(Str::random(5)),
		    'id_client'=>1,
		    'id_delivery'=>1,
		    'id_state'=>3,
		    'id_payment'=>1,
		    'id_driver'=>2,
		    'total_paid'=>rand(10,100),
		    'delivery_date'=>date('Y-m-d'),
		    'delivery_hour'=>'14:00-20:00',
		]);

		\DB::table('orders_products')->insert([
		    ['id_order'=>1, 'id_product'=>1]
		]);
    }
}
