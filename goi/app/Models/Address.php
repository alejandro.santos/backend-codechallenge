<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table 		= 'address';
	protected $primaryKey 	= 'id';
	public 	  $timestamps 	= false;

	public function orders(){
		return $this->hasOne('App\Models\Order');
	}
}
