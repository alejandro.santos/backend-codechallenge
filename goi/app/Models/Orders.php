<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table 		= 'orders';
	protected $primaryKey 	= 'id';

	public function address(){
		return $this->belongsTo('App\Models\Address', 'id_delivery');
	}
}
