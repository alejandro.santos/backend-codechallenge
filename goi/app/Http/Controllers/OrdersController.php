<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\Drivers;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->order  = new Orders();
        $this->driver = new Drivers();
    }

    /**
     * Stores order information
     *
     * @return json
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
        	'id_client'         => 'required|integer',
            'id_client'         => 'required|integer',
            'id_delivery'       => 'required|integer',
            'id_state'          => 'required|integer',
            'id_payment'        => 'required|integer',
            'id_driver'         => 'required|integer',
            'total_paid'        => 'required|numeric',
            'delivery_date'     => 'required|date',
            'delivery_hour'     => 'nullable|string',
            'created_at'     	=> 'nullable|date'
       ]);
        
       if ($validator->fails()) {

           return  response()->json($validator->errors(), 422);

       }else{

       		$this->order->reference  	 = strtoupper(\Str::random(5));
            $this->order->id_client  	 = $request->id_client;
            $this->order->id_delivery    = $request->id_delivery;
            $this->order->id_state       = $request->id_state;
            $this->order->id_payment     = $request->id_payment;
            $this->order->id_driver      = $this->driver::inRandomOrder()->first('id')->id;
            $this->order->total_paid     = $request->total_paid;
            $this->order->delivery_date  = $request->delivery_date;
            $this->order->delivery_hour  = $request->delivery_hour;
            
            if ($this->order->save()){

                return response()->json(array('success' => true, 'id' => $this->order->id));
                
            } 
        }
    }

    /**
     * Display order
     * @param $id (Driver ID), $date (Delivery Date)
     * @return json
     */
    public function tasks($id, $date){

        $tasks = $this->order::where([[ 'id_driver' , $id],['delivery_date' , $date]])->with('address')->get();

        if (count($tasks) > 0) {

            return response()->json($tasks);

        }else{

            return response()->json(array('message' => 'No hay tareas asignadas'));

        }
    }
}
