## Requirements

-Laravel 5.8<br>
-PHP 7<br>
-Apache 2<br>
-MySQL 5.6<br>

## Introduction

Database configuration in database.php 'engine' => 'InnoDB', 'charset' => 'utf8', 'collation' => 'utf8_general_ci'

Migration creation with php artisan:migrate

Models creation (Orders, Address, Drivers, Clients) with php artisan make:model

Sedders creation (Clients, Address y Drivers) with php artisan make:seeder

Factory and Faker creation (Clients, Address, Drivers) with php artisan make:factory

## How to start

Run **php artisan migrate** to create the DB

Run **php artisan db:seed** to create the Seeders

**Insert Order**

function store() located in App\Http\Controllers\OrdersController.php 

Method GET

id_client 		= References DB clients id int<br>
id_delivery 	= References DB delivery id int<br>
id_state 		= References DB order_state id int<br>
id_payment 		= References DB payment_type id int<br>
id_driver 		= References DB drivers id int<br>
total_paid 		= Float<br>
delivery_date 	= Date<br>

Demo: /goi/public/api/store?id_client=1&id_delivery=1&id_state=1&id_payment=1&id_driver=5&total_paid=50.1&delivery_date=2019-08-20

**Get Tasks**

function tasks() located in App\Http\Controllers\OrdersController.php 

Method GET

id 		= ID Driver<br>
date 	= Delivery date<br>

/goi/public/api/tasks/{id}/{date}

Demo: /goi/public/api/tasks/1/2019-08-20

**DB Model**

<img src="https://gitlab.com/alejandro.santos/backend-codechallenge/raw/master/goi/DB-Model.png">